/* global google */
import { useState, useEffect } from 'react';
import jwt_decode from 'jwt-decode'
import './App.css';

function App() {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const [userInfo, setUserInfo] = useState(null);

  const onOneTapSignedIn = (response => {
    setIsSignedIn(true);
    const decodedToken = jwt_decode(response.credential);
    setUserInfo({...decodedToken});
  });
  
  const initializeGSI = () => {
   google.accounts.id.initialize({
       client_id: '143490274328-elmtrg90i17817con62kvtncq9t70jk2.apps.googleusercontent.com',
       login_uri: 'https://epic-booth-68219f.netlify.app',
       callback: onOneTapSignedIn
   });

   google.accounts.id.prompt(notification => {
      if (notification.isNotDisplayed()) {
           console.log(notification.getNotDisplayedReason());
       } else if (notification.isSkippedMoment()) {
           console.log(notification.getSkippedReason());
       } else if(notification.isDismissedMoment()) {
           console.log(notification.getDismissedReason());
       }
       console.log("Prompt Notification Object:",notification);
   });
   
  }
    
  useEffect(() => {
   const script = document.createElement('script');
   script.src = 'https://accounts.google.com/gsi/client';
   script.onload = initializeGSI();
   script.async = true;
   document.querySelector('body').appendChild(script);
  });

  useEffect(() => {
    console.log("User Info Object: ",userInfo);
  });

  const signout = () => {
    // refresh the page
    window.location.reload();
  }

  return (
    <div className="App">
      <header className="App-header">
        { isSignedIn && userInfo ? 
        <div>
          <img style={{borderRadius: '100%'}} src={userInfo.picture} alt="Profile Pic"/><br/>
          <b>Hello</b> <b>{userInfo.given_name}</b> (<em>{userInfo.email}</em>)
          <div className="g_id_signout">
            <button onClick={() => signout()}>Sign Out</button>
          </div>
        </div> : <div>Dear, You are <b>not signed in :(</b></div>}
      </header>
    </div>
  );
}

export default App;
